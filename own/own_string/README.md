# Module own_string

## Description

Ce module nous permet de remanier des chaînes de caractères et d'en extraire un composant précis.

### Fonctions

Dans ce module on possède que 2 fonctions :

* extract_dir() qui extrait d'un path le path du dossier parent.
* extract_file() qui extrait d'un path le nom du fichier.

### Tests

Les tests sont dans le module tests du projet au sein du fichier test_string. On y teste les deux fonction précédente avec 7 lots de données par fonction et où l'on teste tout les retours possibles (aussi bien pour des chemins type 'windows' ou type 'unix').
