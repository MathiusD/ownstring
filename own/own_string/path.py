import logging

"""
    Fonction extract_dir
    %       Fonction qui nous renvoie le dossier parent du chemin
            passé, si ce dernier est déjà la racine, elle renvoie
            alors le chemin passé en argument
    %IN     path: chaine de caractère indiquant le chemin en question
    %OUT    directory : chaine de caractère indiquant le chemin du
                        dossier parent
"""

def extract_dir(path):
    directory = ""
    separator = "/"
    windaube = ":"
    relat = "."
    direct = path.split(separator)
    logging.debug("Path Re-Construction")
    if (len(direct) == 1):
        if (direct[0] == "") or (len(direct[0].split(windaube)) == 2):
            directory = path
        else :
            directory = relat
    else:
        for indice in range(len(direct) - 1):
            directory = directory + direct[indice] + separator
    return directory

"""
    Fonction extract_file
    %       Fonction qui nous renvoie le nom du fichier au sein
            d'un chemin donné
    %IN     path: chaine de caractère indiquant le chemin en question
    %OUT    file_name : chaine de caractère indiquant le nom du fichier
"""

def extract_file(path):
    separator = "/"
    direct = path.split(separator)
    logging.debug("Path Extraction")
    file_name = direct[len(direct)-1]
    return file_name
