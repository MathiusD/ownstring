import unittest
from own.own_string.path import extract_dir, extract_file

class Test_Own_CSV(unittest.TestCase):

    def test_extract_dir(self):
        path_1 = "C:/Example/Blip/1.sqlite"
        path_2 = "/home/mathius/.boardgame.sqlite"
        path_3 = "/run/media/nom_de_périphérique"
        path_4 = "/"
        path_5 = "Z:"
        path_6 = "tests/test_string.py"
        path_7 = "tests"
        path_rep_1 = "C:/Example/Blip/"
        path_rep_2 = "/home/mathius/"
        path_rep_3 = "/run/media/"
        path_rep_4 = "/"
        path_rep_5 = "Z:"
        path_rep_6 = "tests/"
        path_rep_7 = "."
        self.assertEqual(path_rep_1, extract_dir(path_1))
        self.assertEqual(path_rep_2, extract_dir(path_2))
        self.assertEqual(path_rep_3, extract_dir(path_3))
        self.assertEqual(path_rep_4, extract_dir(path_4))
        self.assertEqual(path_rep_5, extract_dir(path_5))
        self.assertEqual(path_rep_6, extract_dir(path_6))
        self.assertEqual(path_rep_7, extract_dir(path_7))
    
    def test_extract_file(self):
        path_1 = "C:/Example/Blip/1.sqlite"
        path_2 = "/home/mathius/.boardgame.sqlite"
        path_3 = "/run/media/nom_de_périphérique"
        path_4 = "/"
        path_5 = "Z:"
        path_6 = "tests/test_string.py"
        path_7 = "tests"
        path_rep_1 = "1.sqlite"
        path_rep_2 = ".boardgame.sqlite"
        path_rep_3 = "nom_de_périphérique"
        path_rep_4 = ""
        path_rep_5 = "Z:"
        path_rep_6 = "test_string.py"
        path_rep_7 = "tests"
        self.assertEqual(path_rep_1, extract_file(path_1))
        self.assertEqual(path_rep_2, extract_file(path_2))
        self.assertEqual(path_rep_3, extract_file(path_3))
        self.assertEqual(path_rep_4, extract_file(path_4))
        self.assertEqual(path_rep_5, extract_file(path_5))
        self.assertEqual(path_rep_6, extract_file(path_6))
        self.assertEqual(path_rep_7, extract_file(path_7))